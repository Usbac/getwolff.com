<?php

namespace Controller;

use Wolff\Core\Cache;
use Wolff\Core\Container;
use Wolff\Core\Controller;
use Wolff\Core\View;

class Changelog extends Controller
{

    private const CACHE_KEY = 'changelog_md';


    public function index()
    {
        if (!Cache::has(self::CACHE_KEY)) {
            $this->setCache(self::CACHE_KEY);
        }

        View::render('changelog', [
            'header'    => View::getRender('header', [ 'page' => 'changelog' ]),
            'changelog' => Cache::get(self::CACHE_KEY),
        ]);
    }


    private function setCache()
    {
        Cache::set(self::CACHE_KEY, Container::get('markdown')->parse(file_get_contents(path('CHANGELOG.md'))));
    }
}
