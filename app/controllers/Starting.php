<?php

namespace Controller;

use Wolff\Core\Cache;
use Wolff\Core\Container;
use Wolff\Core\Controller;
use Wolff\Core\View;

class Starting extends Controller
{

    public function index()
    {
        if (!Cache::has('releases_view')) {
            $cache_data['releases'] = $this->getReleases();
            Cache::set('releases_view', View::getRender('releases', $cache_data));
            Cache::set('current_version', preg_replace('/[a-zA-Z]/', '', $cache_data['releases'][0]['name']));
        }

        View::render('starting', [
            'header'          => View::getRender('header', [ 'page' => 'starting' ]),
            'controller_code' => View::getRender('starting/controller'),
            'route_code'      => View::getRender('starting/route'),
            'view_code'       => View::getRender('starting/view'),
            'releases'        => Cache::get('releases_view'),
            'current_version' => Cache::get('current_version'),
        ]);
    }


    public function getReleases()
    {
        $content = @file_get_contents('http://api.github.com/repos/usbac/wolff/releases', false, stream_context_create([
            'http' => [
                'method' => 'GET',
                'header' => [
                    'User-Agent: PHP',
                ],
            ],
        ]));

        return array_map(function ($release) {
            return [
                'name' => $release['name'],
                'url'  => $release['html_url'],
                'body' => Container::get('markdown')->parse($release['body']),
                'date' => (new \Datetime($release['published_at']))->format('m/d/Y'),
            ];
        }, array_slice(json_decode($content, true), 0, 4));
    }
}
