<?php

namespace Controller;

use Wolff\Core\Cache;
use Wolff\Core\Container;
use Wolff\Core\Controller;
use Wolff\Core\View;

class Documentation extends Controller
{
    const VERSIONS = [ '3', '4' ];

    const PAGES = [
        'authentication' => 'Authentication',
        'cache'          => 'Cache',
        'config'         => 'Config',
        'container'      => 'DI Container',
        'controller'     => 'Controller',
        'db'             => 'Database',
        'faq'            => 'FAQ',
        'file'           => 'Files',
        'home'           => 'Home',
        'installation'   => 'Installation',
        'language'       => 'Language',
        'logging'        => 'Logging',
        'maintenance'    => 'Maintenance',
        'middleware'     => 'Middleware',
        'pagination'     => 'Pagination',
        'request'        => 'Request',
        'response'       => 'Response',
        'routes'         => 'Routes',
        'session'        => 'Session',
        'starting'       => 'Starting',
        'stdlib'         => 'Standard library',
        'string'         => 'String',
        'template'       => 'Template',
        'testing'        => 'Testing',
        'validation'     => 'Validation',
        'view'           => 'View',
    ];


    public function index($request, $response)
    {
        $version = $request->query('version') ?? '';
        $page = $request->query('page') ?? '';

        if (!$this->isValidUrl($version, $page)) {
            $response->setCode(404);
            return;
        }

        $cache_key = "$version/$page";
        if (!Cache::has($cache_key)) {
            $content = file_get_contents("https://raw.githubusercontent.com/Usbac/wolff/$version.x/docs/en/$page.md");
            if ($content) {
                Cache::set($cache_key, Container::get('markdown')->parse($content));
            }
        }

        View::render('documentation', [
            'header'     => View::getRender('header', [ 'page' => 'documentation' ]),
            'title'      => self::PAGES[$page],
            'page'       => $page,
            'prefix_url' => "doc/$version/",
            'body'       => Cache::get($cache_key),
        ]);
    }


    private function isValidUrl($version, $page)
    {
        return in_array($version, self::VERSIONS) && array_key_exists($page, self::PAGES);
    }
}
