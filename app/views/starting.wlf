<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Wolff - Getting started</title>
    {% style="{{ url('font-awesome-4.7.0/css/font-awesome.min.css') }}" %}
    {% style="{{ url('assets/css/styles.css') }}" %}
    {% icon="{{ url('favicon.ico') }}" %}
</head>
<body>

{! $header !}

<div class='wlf-container'>

    {# Download #}

    <div class="download-start">
        <h1>Download</h1>
        <a href="https://github.com/Usbac/wolff/releases/download/v{{ $current_version }}/wolff-bundle.zip"><button class="download-btn">Version {{ $current_version }}</button></a>
        <div>
            <div class="download-option">
                <img src="{{ url('github.png') }}"/>
                <h2>Github</h2>
                <pre id="github" onClick="copyLink('github', 'git clone https://github.com/Usbac/wolff.git')" class="dark-code-block line-code">git clone https://github.com/Usbac/wolff.git</pre>
            </div>
            <div class="download-option">
                <img src="{{ url('composer.png') }}"/>
                <h2>Composer</h2>
                <pre id="composer" onClick="copyLink('composer', 'composer create-project usbac/wolff')" class="dark-code-block line-code">composer create-project usbac/wolff</pre>
            </div>
        </div>
    </div>

    {# Starting #}

    <div class="starting-tutorial">
        <h2>Let's build something together</h2>
        <div>
            <div>
                <pre class="dark-code-block code-path code-small">
                    <div>system > web.php</div>
                    <div>{! $route_code !}</div>
                </pre>
                <p>Create a route</p>
            </div>

            <div>
                <pre class="dark-code-block code-path code-small">
                    <div>app > controllers > Home.php</div>
                    <div>{! $controller_code !}</div>
                </pre>
                <p>Create a controller</p>
            </div>

            <div>
                <pre class="dark-code-block code-path code-small">
                    <div>app > views > home.wlf</div>
                    <div>{! $view_code !}</div>
                </pre>
                <p>Design the view</p>
            </div>
        </div>
    </div>

    {# Requirements #}

    <div class="starting-features">
        <div>
            <h2>Latest releases</h2>
            {! $releases !}
        </div>

        <div>
            <h2>Requirements</h2>
            <ul>
                <li>PHP 7.1 or higher</li>
                <li>Composer</li>
            </ul>
            <h2>Contact</h2>
            <p>Requests and questions can be send to the email <a href="mailto:contact@getwolff.com">contact@getwolff.com</a>, you will be contacted shortly.</p>
            <h2>Contributing</h2>
            <p>Any contribution or support to this project in the form of a pull request will be highly appreciated.
            This can be done in both of the official repositories <a href="https://github.com/Usbac/wolff" target="_blank">wolff</a> and <a href="https://github.com/Usbac/wolff-framework" target="_blank">wolff-framework</a>.</p></div>
        </div>
    </div>

</div>
</body>
</html>

@include('footer')


<script>
var timeout = 2000;
var message = 'Copied to clipboard!';

function copy(id) {
    let target = document.getElementById(id);
    let range, select;

    if (document.createRange) {
        range = document.createRange();
        range.selectNode(target)
        select = window.getSelection();
        select.removeAllRanges();
        select.addRange(range);
        document.execCommand('copy');
        select.removeAllRanges();
    } else {
        range = document.body.createTextRange();
        range.moveToElementText(target);
        range.select();
        document.execCommand('copy');
    }
}

function copyLink(el, text) {
    copy(el);
    document.getElementById(el).innerHTML = message;
    setTimeout(function() {
        document.getElementById(el).innerHTML = text;
    }, timeout);
}
</script>
