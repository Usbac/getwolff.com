<?php

return [
    'language'       => 'english',
    'log_on'         => true,
    'development_on' => true,
    'template_on'    => true,
    'cache_on'       => true,
    'stdlib_on'      => true,
    'maintenance_on' => false
];
