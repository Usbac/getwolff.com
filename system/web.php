<?php

use Wolff\Core\Cache;
use Wolff\Core\Container;
use Wolff\Core\Route;
use Wolff\Core\View;

Container::singleton('markdown', new \cebe\markdown\GithubMarkdown());

/**
 * Pages
 */

Route::get('/', function () {
    if (!Cache::has('current_version')) {
        $cache_data['releases'] = Wolff\Core\Controller::method('Starting', 'getReleases');
        Cache::set('current_version', preg_replace('/[a-zA-Z]/', '', $cache_data['releases'][0]['name']));
    }

    View::render('home', [
        'header'          => View::getRender('header', [ 'page' => 'home' ]),
        'code'            => View::getRender('code'),
        'current_version' => Cache::get('current_version'),
    ]);
});

Route::get('changelog', [ Controller\Changelog::class, 'index' ]);

Route::get('starting', [ Controller\Starting::class, 'index' ]);

Route::get('doc/{version}/{page}', [ Controller\Documentation::class, 'index' ]);

/* Codes */

Route::code(404, function () {
    View::render('404', [
        'header' => View::getRender('header'),
    ]);
});

/* Easter egg pages */

Route::get('418', function ($req, $res) {
    $res->setCode(418);
    View::render('418', [
        'header' => View::getRender('header'),
    ]);
});

Route::view('laravel', 'laravel', [
    'header' => View::getRender('header'),
]);
